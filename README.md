# TelegramBot

[![pipeline status](https://gitlab.com/Aleksandr909/telegram-bot/badges/master/pipeline.svg)](https://gitlab.com/Aleksandr909/telegram-bot/-/commits/master)

## [Бот](https://t.me/some_4_test_bot)

### О боте

Бот сделан на новой библиотеке - [Urban bot](https://github.com/urban-bot/urban-bot). Умеет бросать монетку, показывать погоду в городе, говорить случайное число, совсем немного общаться. Также по пути /work - можно немного познакомиться со мной. Подробнее можно узнать у самого бота, прописав /help.

Бот размещен на сервере Vscale с Ubuntu OS. С помощью **[gitlab ci](https://gitlab.com/Aleksandr909/telegram-bot/-/blob/master/.gitlab-ci.yml)** проект автоматически собирается на серверах gitlab и затем переносится на сервер. На сервере с помощью **nginx** приложение проксируется с порта на нужный поддомен.

### Локальное тестирование

```sh
git clone https://gitlab.com/Aleksandr909/telegram-bot.git
cd telegram-bot
npm install
npm run dev
```

### Лицензия

[BSD 2-clause "Simplified" License](https://gitlab.com/Aleksandr909/telegram-bot/-/blob/master/LICENSE)
