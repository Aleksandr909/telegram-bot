import React, { useState } from 'react'
import fs from 'fs'
import {
  Route, Router, Text, ButtonGroup, Button, useText, Image,
} from '@urban-bot/core'
import axios from 'axios'
import logo from './assets/logo.png'
import randomInteger from './core/functions/random'
import Work from './Pages/Work'
import capitalize from './core/functions/capitalize'
import Help from './Pages/Help'

function Echo() {
  const [stateText, setStateText] = useState('Скажи что-нибудь')

  useText(({ text }) => {
    setStateText(text)
  })

  return (
    <Text simulateTyping={2000}>
      <i>{stateText}</i>
    </Text>
  )
}

const getWeather = async (cityQuery: string) => {
  const [city] = cityQuery.split(' ').slice(-1)
  try {
    const cityRes = await axios.get(
      encodeURI(
        `https://api.openweathermap.org/data/2.5/weather?q=${city}&lang=ru&appid=03036376128eae850608efc5c7aa0f8b`,
      ),
    )
    const weather = `${capitalize(cityRes.data.weather[0].description)}, ${Math.round(
      cityRes.data.main.temp - 273.5,
    )}°C. Скорость ветра - ${cityRes.data.wind.speed} м/с`
    return weather
  } catch (e) {
    console.log(e.message)
    return 'Город не найден'
  }
}

const data: { [key: string]: string[] } = {
  Привет: ['Здарова', 'Привет', 'Добрый день'],
  Здарова: ['Здарова', 'Привет', 'Добрый день'],
  'Как дела?': ['Хорошо, как твои?'],
  'Что делаешь?': ['Ничего, а ты?'],
}
function Some() {
  const [stateText, setStateText] = useState({ text: '' })
  const setStateTextHandler = (text: string) => {
    setStateText({ text })
  }
  useText(({ text }) => {
    if (text.toLowerCase().includes('погода')) {
      getWeather(text).then(setStateTextHandler)
    } else if (text.toLowerCase().includes('число от')) {
      const [from, to] = text.split(/([1-9])/).filter((e) => !isNaN(+e))
      setStateTextHandler(`${randomInteger(+from, +to)}`)
    } else if (text.toLowerCase().includes('монетк')) {
      const variants = ['Орел', 'Решка']
      setStateTextHandler(variants[randomInteger(0, 1)])
    } else if (data[text]) {
      const resLength = data[text]?.length || 0
      const random = resLength > 1 ? randomInteger(0, resLength - 1) : 0
      setStateTextHandler(data[text][random] || 'Я даже не знаю, что на это ответить')
    }
  })
  if (!stateText.text) return <></>

  return <Text simulateTyping={2000}>{stateText.text}</Text>
}

function Logo() {
  const [title, setTitle] = useState('Urban Bot')

  const addRobot = () => {
    setTitle(`${title}🤖`)
  }

  return (
    <Image
      title={title}
      file={fs.createReadStream(logo)}
      buttons={(
        <ButtonGroup>
          <Button onClick={addRobot}>Add robot</Button>
        </ButtonGroup>
              )}
    />
  )
}

export default function App() {
  return (
    <>
      {/* <Answers /> */}
      {/* <Text>Welcome to Urban Bot! Type /echo or /logo.</Text> */}
      <Router>
        <Route path="/">
          <Some />
        </Route>
        <Route path="/echo">
          <Echo />
        </Route>
        <Route path="/logo">
          <Logo />
        </Route>
        <Route path="/work">
          <Work />
        </Route>
        <Route path="/help">
          <Help />
        </Route>
      </Router>
    </>
  )
}
