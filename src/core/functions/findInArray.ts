const findInArray = (arr: string[], text: string) => (
  !!arr.find((e) => text.toLowerCase().includes(e.toLowerCase()))
)
export default findInArray
