const capitalize = (str: string) => {
  const [first] = str
  return first.toUpperCase() + str.slice(1)
}
export default capitalize
