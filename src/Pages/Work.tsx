import React, { useState } from 'react'
import { useText, Text } from '@urban-bot/core'
import findInArray from '../core/functions/findInArray'

const Experience = () => (
  <>
    Реализованные проекты:
    <br />
    <a href="https://pomogu.org/">Pomogu</a>
    <br />
    <a href="https://test.arcadia-vr.com/">Arcadia</a>
    <br />
    <a href="https://app.jobbix.pro/sign-in">Jobbix</a>
    <br />
    <a href="https://pokedex.vorfolio.ru/">Pokedex</a>
    <br />
    <a href="https://shorelin.vorfolio.ru/">Shorelin</a>
    <br />
    <a href="https://www.vorfolio.ru/">Vorfolio</a>
  </>
)

export function Answers() {
  const [stateText, setStateText] = useState<string | React.ReactNode>('')

  useText(({ text }) => {
    console.log('findInArray')

    if (findInArray(['Проект', 'Работ', 'Опыт'], text)) setStateText(Experience)
  })
  if (!stateText) return <></>
  return (
    <Text simulateTyping={2000}>
      {stateText}
    </Text>
  )
}
const Work = () => (
  <>
    <Text simulateTyping={1000}>
      Добрый день
    </Text>
    <Text simulateTyping={2000}>
      Привет. Меня зовут Александр и я веб разработчик.
      Весь мой коммерческий опыт - разработка SPA на React.
      В основном при разработке использую Redux, Material-ui.
      <br />
      Вы можете ознакомиться со мной на моем
      {' '}
      <a href="https://www.vorfolio.ru">сайте</a>
      .
      Посмотреть мой код можно в
      {' '}
      <a href="https://gitlab.com/Aleksandr909/">gitlab</a>
      .
    </Text>
    <Answers />
  </>
)

export default Work
