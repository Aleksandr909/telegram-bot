/* eslint-disable react/no-unescaped-entities */
import React from 'react'
import { Text } from '@urban-bot/core'

const Help = () => (
  <>
    <Text simulateTyping={200}>
      <b>Освновные команды:</b>
      <br />
      /work - Знакомство с автором
      <br />
      /workHelp - Команды при знакомстве с автором
      <br />
      /echo - Режим попугая
      <br />
      <b>Дополнительные возможности:</b>
      <br />
      <i>Погода</i>
      {' '}
      - Погода в нужном городе (например "погода Москва")
      <br />
      <i>Случайное число</i>
      {' '}
      - Случайное число в нужном промежутке (например "число от 17 до 43")
      <br />
      <i>Монетка</i>
      {' '}
      - Орел или решка (например "брось монетку")
      <br />
    </Text>
  </>
)

export default Help
