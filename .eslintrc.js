module.exports = {
    parser: '@typescript-eslint/parser',
    extends: [
        'eslint:recommended',
        'plugin:import/recommended',
        'plugin:react/recommended',
        'plugin:react-hooks/recommended',
        'plugin:jest/recommended',
        'plugin:@typescript-eslint/recommended',
        "airbnb"
    ],
    plugins: ['@typescript-eslint', 'react', 'jest', 'react-hooks'],
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module',
        project: './tsconfig.json',
        tsconfigRootDir: '.',
        ecmaFeatures: {
            jsx: true,
        },
    },
    env: {
        node: true,
        es6: true,
        jest: true,
    },
    rules: {
        'react/prop-types': 'off',
        'no-unused-vars': 'off',
        '@typescript-eslint/no-unused-vars': ['error', { varsIgnorePattern: '_.+', argsIgnorePattern: '_.+' }],
        '@typescript-eslint/no-use-before-define': 'off',
        '@typescript-eslint/explicit-function-return-type': 'off',
        '@typescript-eslint/no-empty-function': 'off',
        "linebreak-style": [
            "error",
            "unix"
        ],
        "consistent-return": "off",
        "camelcase": "off",
        "no-underscore-dangle": "off",
        "no-restricted-syntax": "off",
        "no-await-in-loop": "off",
        "no-unused-vars": "off",
        "semi": [
            "error",
            "never"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "import/no-unresolved": "off",
        "import/no-extraneous-dependencies": "off",
        "import/extensions": "off",
        "import/first": "off",
        "@typescript-eslint/semi": ["error", "never"],
        "no-use-before-define": "off",
        "react/jsx-filename-extension": [2, { "extensions": [".jsx", ".tsx"] }],
        "react/jsx-props-no-spreading": "off",
        "react/require-default-props": "off",
        "no-restricted-globals": "warn"
    },
    settings: {
        'import/resolver': {
            node: {
                extensions: ['.js', '.jsx', '.ts', '.tsx'],
            },
        },
        react: {
            version: 'detect',
        },
    },
    ignorePatterns: ['webpack.config.js'],
};
